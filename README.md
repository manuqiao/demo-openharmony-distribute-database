# demo-openharmony-distribute-database

#### 介绍
分布式数据库kv store demo。\
log看效果。

#### 开发环境

dayu200\
openharmony 3.2.12.3\
api 9

#### 使用说明
1. 使用设备1运行app
2. 创建KvManager
3. 创建KvStore
4. 点击其他按钮查看效果

订阅可以监听分布式数据库变化。\
写入会把count值写进数据库，写成功以后count加一。\
读取将把count值读出来。

5. 在使用openharmony设备2运行app。执行步骤1和2。
6. 设备2上点击读取会读取失败。需要先点击同步按钮，从设备1同步数据。